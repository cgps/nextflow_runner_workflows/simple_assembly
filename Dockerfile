FROM continuumio/miniconda3:4.6.14
LABEL authors="Anthony Underwood" \
      description="Docker image containing all requirements for assembly pipeline"

# install dependencies via conda
COPY environment.yml /
RUN conda env create -f /environment.yml && conda clean -a

# point PATH at conda bin
ENV PATH /opt/conda/envs/assembly/bin:$PATH